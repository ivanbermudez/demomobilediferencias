package apkDemo.appium;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.text.html.parser.Element;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.xml.sax.SAXException;

import io.appium.java_client.MobileElement;

/**
 * Hello world!
 *
 */
public class App {
	static String fecha;

	public static void main(String[] args) {
		//ejecutarWeb();
		ejecutarMobile();
	}
	
	public static void ejecutarWeb() {
		App aplicacion = new App("web");
		webDemo mbd = new webDemo(fecha);
		mbd.inicializarDriver();
		mbd.revisarPaginaWeb();
		aplicacion.finalizarAplicacion();
	}
	
	public static void ejecutarMobile() {
		App aplicacion=new App("web"); 
		mobileDemo md=new mobileDemo(fecha);
		md.revisarPaginaMobile(); 
		md.realizarAcciones();
		aplicacion.finalizarAplicacion();
	}

	public App(String carpeta) {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-hhmm");
		Date date = new Date();
		fecha = dateFormat.format(date);
		new File("logs/" + fecha + "").mkdir();
		PrintStream out = null;
		try {
			out = new PrintStream(new FileOutputStream("logs/" + fecha + "/logon.txt"));
		} catch (FileNotFoundException e) {
			System.err.println("No pude generar archivo de logs " + e.getMessage());
		}
		// ENVIA AL ARCHIVO DE TEXTO
		System.setOut(out);
		System.setErr(out);
	}

	public void finalizarAplicacion() {
		DateFormat dateFormat2 = new SimpleDateFormat("yyyyMMdd-hhmmss");
		Date date2 = new Date();
		System.out.println("Finalizando ejecución a las " + dateFormat2.format(date2));
	}

}
